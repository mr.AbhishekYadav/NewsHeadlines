//
//  ViewController.swift
//  Headlines
//
//  Created by Abhishek Yadav on 25/07/23.
//

import UIKit

class NewsListingViewController: UIViewController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mTableView: UITableView!
    var newsData: [Article] = []
    var loader: UIActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetup()
    }
    private func initialSetup() {
        self.mTableView.register(UINib(nibName: NewsHeadlinesTableViewCell.xibName, bundle: nil), forCellReuseIdentifier: NewsHeadlinesTableViewCell.cellIdentifier)
        mTableView.delegate = self
        mTableView.dataSource = self
        mTableView.separatorEffect = .none
        fetchNewsData()
    }
    func showLoader() {
        if loader == nil {
            loader = UIActivityIndicatorView(style: .large)
            loader?.center = view.center
            view.addSubview(loader!)
        }
        loader?.startAnimating()
    }
    
    func hideLoader() {
        loader?.stopAnimating()
        loader?.removeFromSuperview()
        loader = nil
    }
}

extension NewsListingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 274.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsHeadlinesTableViewCell.cellIdentifier, for: indexPath) as? NewsHeadlinesTableViewCell
        cell?.setData(dataModel: self.newsData[indexPath.row])
        return cell ?? UITableViewCell()
    }
}
extension NewsListingViewController {
    
    func fetchNewsData() {
        self.showLoader()
        let apiKey = "f5794f51864c4f2cbb861d3dc7d142ed"
//        let currentDate = getCurrentDateFormatted()
        let previousDate = getPreviousDateFormatted()
        guard let url = URL(string: "https://newsapi.org/v2/everything?q=tesla&from=\(previousDate)&sortBy=publishedAt&apiKey=\(apiKey)") else {
            print("Invalid API URL.")
            self.hideLoader()
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("Invalid response or data.")
                return
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601 // Set the date decoding strategy
                    
                    let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
                    
                    if let articlesArray = jsonObject["articles"] as? [[String: Any]] {
                        let articles = try articlesArray.map { try decoder.decode(Article.self, from: JSONSerialization.data(withJSONObject: $0, options: [])) }
                        self.newsData = articles
                        DispatchQueue.main.async {
                            self.mTableView.reloadData()
                            if self.newsData.isEmpty {
                                print("No articles found.")
                            }
                            self.hideLoader()
                        }
                    } else {
                        DispatchQueue.main.async {
                            print("Error accessing 'articles' key or it's not an array.")
                            self.hideLoader()
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        print("Error decoding JSON: \(error)")
                        self.hideLoader()
                    }
                }
            }
        }.resume()
    }
    func getPreviousDateFormatted() -> String {
        let calendar = Calendar.current
        let currentDate = Date()
        
        if let previousDate = calendar.date(byAdding: .day, value: -2, to: currentDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.string(from: previousDate)
        } else {
            // Error handling in case the date calculation fails
            return ""
        }
    }
    func getCurrentDateFormatted() -> String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: currentDate)
    }
    func stringToDate(_ dateString: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)
    }
}

