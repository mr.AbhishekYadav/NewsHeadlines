//
//  DataModel.swift
//  Headlines
//
//  Created by Abhishek Yadav on 25/07/23.
//

import Foundation
struct Article: Codable {
    var source: Source?
    var author: String?
    var title: String?
    var description: String?
    var url: URL?
    var urlToImage: String?
    var publishedAt: Date?
    var content: String?
}
struct Source: Codable {
    var id: String?
    var name: String
}
