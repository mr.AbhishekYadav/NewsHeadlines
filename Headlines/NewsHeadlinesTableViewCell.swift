//
//  NewsHeadlinesTableViewCell.swift
//  Headlines
//
//  Created by Abhishek Yadav on 25/07/23.
//

import UIKit
import SDWebImage
class NewsHeadlinesTableViewCell: UITableViewCell {
    static let cellIdentifier = "NewsHeadlinesTableViewCell"
    static let xibName = "NewsHeadlinesTableViewCell"
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var authLbl: UILabel!
    @IBOutlet weak var headlinesLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension NewsHeadlinesTableViewCell {
    func setData(dataModel: Article) { // , index: Int?) {
        self.headlinesLbl.text = dataModel.title ?? String()
        self.authLbl.text = dataModel.source?.name ?? String()
        
        // Format and display the date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let publishedDate = dataModel.publishedAt {
            let dateString = dateFormatter.string(from: publishedDate)
            self.dateLbl.text = dateString
        } else {
            dateLbl?.text = "N/A" // Set a default value if the date is not available
        }

        // Load the image asynchronously using SDWebImage
        if let imageUrlString = dataModel.urlToImage, let imageUrl = URL(string: imageUrlString) {
            imgView?.sd_setImage(with: imageUrl, completed: nil)
            imgView.contentMode = .scaleToFill
        } else {
            imgView?.image = nil // Set a default image if the URL is not valid or available
        }
    }

}

